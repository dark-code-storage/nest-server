## 📦 使用

$ yarn

$ yarn start:dev

## 🖥 配置

查看 nestjs
<https://docs.nestjs.com/>

## ⌨️ 环境要求

- node >= v16.0.0 < v17.0.0
- npm = v7.18.1
- mongodb >= v6.0.0
- redis >= v7.0.0
- yarn >= v1.22.0
- yarn 源地址：<https://registry.npmjs.org/>

## 🧰 技术概览

- 开发语言：JavaScript
- 开发框架：NestJs
- 数据库：MongoDB
- 缓存：Redis
- 鉴权：Jwt

## 📁 文件夹结构

- /common: 公共业务处理
- /config: 公共配置