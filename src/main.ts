import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DEFAULT_PORT, DEFAULT_PREFIX } from './common/constants/config';
import { ConfigService } from '@nestjs/config';
import { WsAdapter } from './common/adapter/ws.adapter';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);

    // 请求prefix
    app.setGlobalPrefix(DEFAULT_PREFIX);

    //WebSocket
    app.useWebSocketAdapter(new WsAdapter(app, app.get(ConfigService)));

    //监听端口
    await app.listen(DEFAULT_PORT);
}
bootstrap();
