import * as chalk from 'chalk';
const log = console.log;

const error = (title: string, message: string) => {
    console.log(chalk.bgGray(title) + chalk.red(message));
};

const success = (title: string, message: string) => {
    log(chalk.bgGray(title) + chalk.green(message));
};

const warn = (title: string, message: string) => {
    log(chalk.bgGray(title) + chalk.yellow(message));
};
const info = (title: string, message: string) => {
    log(chalk.bgGray(title) + chalk.blue(message));
};

export default {
    error,
    success,
    warn,
    info,
};
