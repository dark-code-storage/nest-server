export const getConfiguration = () =>
    ({
        jwt: {
            secret: process.env.JWT_SECRET,
        },
        database: {
            url: process.env.DB_URL,
            name: process.env.DB_NAME,
            user: process.env.DB_USER,
            pass: process.env.DB_PASSWORD,
        },
        ws: {
            port: process.env.WS_PORT,
            path: process.env.WS_PATH,
        },
        redis: {
            config: {
                url: process.env.REDIS_URL,
                password: process.env.REDIS_PASSWORD,
            },
        },
    }) as const;
export type ConfigurationType = ReturnType<typeof getConfiguration>;
export type ConfigurationKeyPaths = Record<any, any>;
