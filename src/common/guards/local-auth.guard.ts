import { Observable } from 'rxjs';
import { AuthGuard } from '@nestjs/passport';
import { ExecutionContext, Injectable } from '@nestjs/common';
/**
 * @description: 校验用户密码守卫;
 */
@Injectable()
export class LocalAuthGuard extends AuthGuard('local') {
    context: ExecutionContext;
    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        this.context = context;
        return super.canActivate(context);
    }

    // //主动处理错误,进行日志记录
    handleRequest(err, user) {
        if (err || !user) {
            throw err;
        }
        return user;
    }
}
