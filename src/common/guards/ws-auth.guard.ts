import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Socket } from 'socket.io';
import { CustomSocket } from '../adapter/ws.adapter';
import { ApiException } from '../class/ajax-error-result';

@Injectable()
export class WsGuard implements CanActivate {
    constructor(private jwtService: JwtService) {}

    canActivate(context: ExecutionContext): boolean | Promise<boolean> {
        const client = context.switchToWs().getClient<Socket>() as CustomSocket;
        const token = this.extractToken(client);

        if (!token) {
            client.emit('auth-error', { error: '无效的登录信息' });
            throw new ApiException(12001);
        }

        try {
            const user = this.jwtService.verify(token);
            client.user = user; // 将用户信息挂载到socket上,方便后续使用
            return true;
        } catch (e) {
            client.emit('auth-error', { error: '登录无效' });
            throw new ApiException(12001);
        }
    }

    private extractToken(client: Socket): string | null {
        const token = client.handshake?.query?.token;
        if (!token) {
            return null;
        }
        return token as string;
    }
}
