import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { ApiException } from '../class/ajax-error-result';
import { PUBLIC_KEY } from '../constants/decorator.constant';
import { ExecutionContext, Injectable } from '@nestjs/common';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
    constructor(private reflector: Reflector) {
        super();
    }
    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        // getHandler 值将覆盖 getClass上面的值
        const noInterception = this.reflector.getAllAndOverride(PUBLIC_KEY, [context.getHandler(), context.getClass()]);
        if (noInterception) return true;

        return super.canActivate(context);
    }

    // 主动处理错误
    handleRequest(err, user) {
        if (err || !user) {
            throw err || new ApiException(12001);
        }
        return user;
    }
}
