import { isEqual } from 'lodash';
import { Request } from 'express';
import { Reflector } from '@nestjs/core';
import { ApiException } from '../class/ajax-error-result';
import { InjectRedis, Redis } from '@nestjs-modules/ioredis';
import { REPEAT_SUBMIT_METADATA } from '../constants/decorator.constant';
import { RepeatSubmitOption } from '../decorators/repeat-submit.decorator';
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
/**
 * @description: 防止重复提交守卫;
 */
@Injectable()
export class RepeatSubmitGuard implements CanActivate {
    constructor(
        private readonly reflector: Reflector,
        @InjectRedis() private readonly redis: Redis,
    ) {}
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const repeatSubmitOption: RepeatSubmitOption = this.reflector.get(REPEAT_SUBMIT_METADATA, context.getHandler());
        if (!repeatSubmitOption) return true;
        const request: Request = context.switchToHttp().getRequest();
        const cache = await this.redis.get(request.url);
        const data = {
            url: request.url,
        };
        const dataString = JSON.stringify(data);
        if (!cache) {
            //没有缓存数据
            if (dataString) {
                await this.redis.set(request.url, dataString, 'EX', repeatSubmitOption.interval);
            }
        } else {
            if (dataString && isEqual(cache, dataString)) {
                throw new ApiException(12003);
            }
        }
        return true;
    }
}
