import { HttpException, HttpStatus } from '@nestjs/common';
import { ErrorCodeMap, ErrorCodeMapType } from '../constants/error-code.constants';

/**
 *  @description: Api业务异常均抛出该异常
 */
export class ApiException extends HttpException {
    //业务类型错误代码，非Http code
    private errorCode: ErrorCodeMapType;

    constructor(errorCode: ErrorCodeMapType) {
        // status 状态码200 正常返回
        super(ErrorCodeMap[errorCode], HttpStatus.OK);
        this.errorCode = errorCode;
    }

    getErrorCode(): ErrorCodeMapType {
        return this.errorCode;
    }
}
