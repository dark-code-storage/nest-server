import { DEFAULT_SUCCESS_CODE } from '../constants/error-code.constants';

export class AjaxResult {
    readonly code: number;
    readonly msg: string;
    readonly data: any;

    constructor(code, msg, data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    static success(data = null, msg = '请求成功') {
        return new AjaxResult(DEFAULT_SUCCESS_CODE, msg, data);
    }

    static error(msg = '请求失败', code = 0) {
        return new AjaxResult(code, msg, null);
    }
}
