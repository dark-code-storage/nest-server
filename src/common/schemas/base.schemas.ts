import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
export class BaseSchema {
    @Prop({
        default: Date.now(),
    })
    createdTime: number;

    @Prop({
        default: Date.now(),
    })
    updatedTime: number;

    @Prop()
    createdBy: string;

    @Prop()
    updatedBy: string;
}
