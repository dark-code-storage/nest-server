import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Reflector } from '@nestjs/core';
import { AjaxResult } from '../class/ajax-result';
import { KEEP_KEY } from '../constants/decorator.constant';
import { Injectable, NestInterceptor, ExecutionContext, CallHandler, HttpStatus } from '@nestjs/common';

@Injectable()
export class ResponseTransformInterceptor implements NestInterceptor {
    constructor(private readonly reflector: Reflector) {}

    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        return next.handle().pipe(
            map((data) => {
                const response = context.switchToHttp().getResponse();
                const keep = this.reflector.getAllAndOverride<boolean>(KEEP_KEY, [
                    context.getHandler(),
                    context.getClass(),
                ]);
                if (keep) return data;
                response.status(HttpStatus.OK);
                response.header('Content-Type', 'application/json; charset=utf-8');
                return AjaxResult.success(data);
            }),
        );
    }
}
