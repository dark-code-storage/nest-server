import { isArray } from 'lodash';
import { AjaxResult } from '../class/ajax-result';
import { ApiException } from '../class/ajax-error-result';
import { DEFAULT_ERROR_CODE, DEFAULT_ERROR_MESSAGE } from '../constants/error-code.constants';
import { ExceptionFilter, Catch, ArgumentsHost, HttpException, HttpStatus } from '@nestjs/common';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
    catch(exception: HttpException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse(); // 获取响应对象
        const { result } = this.errorResult(exception);
        response.header('Content-Type', 'application/json; charset=utf-8');
        response.json(result);
    }

    //解析错误类型，获取状态码和返回值
    errorResult(exception: HttpException) {
        //状态统一返回200
        const status = HttpStatus.OK;
        const code =
            exception instanceof ApiException ? (exception as ApiException).getErrorCode() : DEFAULT_ERROR_CODE;

        let message = DEFAULT_ERROR_MESSAGE;
        if (exception instanceof HttpException) {
            const response = exception.getResponse();
            message = (response as any)?.message ?? response;
        }
        if (isArray(message)) {
            message = message?.[0];
        }
        return {
            status,
            result: AjaxResult.error(message, code),
        };
    }
}
