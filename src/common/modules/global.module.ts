import { Module, Global, ValidationPipe } from '@nestjs/common';
import { ConfigurationKeyPaths } from '@/config';
import { MongooseModule } from '@nestjs/mongoose';
import { RedisModule } from '@nestjs-modules/ioredis';
import { JwtAuthGuard } from '../guards/jwt-auth.guard';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { RepeatSubmitGuard } from '../guards/repeat-submit.guard';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { AllExceptionsFilter } from '../providers/all-exception-filter';
import { ResponseTransformInterceptor } from '../providers/response-interceptor';

@Global()
@Module({
    imports: [
        //连接mongoDB数据库
        MongooseModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: (configService: ConfigService<ConfigurationKeyPaths>) => ({
                uri: configService.get<string>('database.url'),
                dbName: configService.get<string>('database.name'),
                user: configService.get<string>('database.user'),
                pass: configService.get<string>('database.pass'),
                useNewUrlParser: true,
                useUnifiedTopology: true,
            }),
            inject: [ConfigService],
        }),

        //连接Redis
        RedisModule.forRootAsync({
            useFactory: (configService: ConfigService) => configService.get('redis'),
            inject: [ConfigService],
        }),
    ],
    providers: [
        //全局参数校验管道
        {
            provide: APP_PIPE,
            useValue: new ValidationPipe({
                whitelist: true, // 启用白名单，dto中没有声明的属性自动过滤
                transform: true, // 自动类型转换
            }),
        },
        // 全局返回值转化拦截器
        {
            provide: APP_INTERCEPTOR,
            useClass: ResponseTransformInterceptor,
        },
        //全局异常过滤器
        {
            provide: APP_FILTER,
            useClass: AllExceptionsFilter,
        },
        // jwt守卫
        {
            provide: APP_GUARD,
            useClass: JwtAuthGuard,
        },
        //阻止连续提交守卫
        {
            provide: APP_GUARD,
            useClass: RepeatSubmitGuard,
        },
    ],
})
export class GlobalModule {}
