/**
 * @description: 统一错误代码定义 ;
 */

export const ErrorCodeMap = {
    // 业务相关
    11001: '账户不存在',
    11002: '用户名或密码错误',

    // token，请求相关
    12001: '登录无效或无权限访问',
    12002: '登录身份已过期',
    12003: '请求过于频繁',
} as const;

export type ErrorCodeMapType = keyof typeof ErrorCodeMap;

/**
 * @description 默认请求成功code码
 */
export const DEFAULT_SUCCESS_CODE = 10000;

/**
 * @description 默认异常错误code码
 */
export const DEFAULT_ERROR_CODE = 10001;

/**
 * @description 默认异常返回信息
 */
export const DEFAULT_ERROR_MESSAGE = '服务端错误，请稍后再试';
