/** 装饰器Key */

//保持原数据返回
export const KEEP_KEY = 'common:keep';

// 无需登录,不进行 jwt 校验
export const PUBLIC_KEY = 'common:public';

// 防止重复提交
export const REPEAT_SUBMIT_METADATA = 'common:repeatSubmit';

// 操作权限标识
export const PERMISSION_KEY_METADATA = 'common:permission';
