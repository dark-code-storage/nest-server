/** 系统配置 */

/**
 * @description 默认端口
 */
export const DEFAULT_PORT = 8080;

/**
 * @description 默认请求前缀
 */
export const DEFAULT_PREFIX = 'api';
