/**
 * @description 成功连接
 */
export const DISPATCH_EVENT_ONLINE = 'online';
/**
 * @description 断开连接
 */
export const DISPATCH_EVENT_OFFLINE = 'offline';
/**
 * @description 发送公告
 */
export const DISPATCH_EVENT_NOTICE = 'notice';

/**
 * @description 接收公告
 */
export const RECEPTION_EVENT_NOTICE = 'notice';
