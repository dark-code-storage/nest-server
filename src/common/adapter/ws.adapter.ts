import { Injectable, INestApplicationContext } from '@nestjs/common';
import { IoAdapter } from '@nestjs/platform-socket.io';
import { Socket, ServerOptions } from 'socket.io';
import { ConfigService } from '@nestjs/config';
import { ObjectId } from 'mongoose';

interface User {
    username: string;
    _id: ObjectId;
}
export interface CustomSocket extends Socket {
    user: User;
}

@Injectable()
export class WsAdapter extends IoAdapter {
    constructor(
        private app: INestApplicationContext,
        private configService: ConfigService,
    ) {
        super(app);
    }

    createIOServer(port: number, options?: ServerOptions) {
        port = this.configService.get<number>('ws.port');
        options.path = this.configService.get<string>('ws.path');
        options.cors = {
            origin: '*',
        };
        const server = super.createIOServer(port, { ...options });
        return server;
    }
}
