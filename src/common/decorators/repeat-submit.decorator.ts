/**
 * @description: 防止重复提交装饰器 ;
 */

import { SetMetadata } from '@nestjs/common';
import { REPEAT_SUBMIT_METADATA } from '../constants/decorator.constant';

export class RepeatSubmitOption {
    interval?: number = 5; //默认5s
}

export const RepeatSubmit = (option?: RepeatSubmitOption) => {
    const repeatSubmitOption = Object.assign(new RepeatSubmitOption(), option);
    return SetMetadata(REPEAT_SUBMIT_METADATA, repeatSubmitOption);
};
