/**
 * @description:  允许无token请求的装饰器;
 */

import { SetMetadata } from '@nestjs/common';
import { PUBLIC_KEY } from '../constants/decorator.constant';

export const Public = () => SetMetadata(PUBLIC_KEY, true);
