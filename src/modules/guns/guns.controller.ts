import { Controller, Get } from '@nestjs/common';
import { GunsService } from './guns.service';

@Controller('guns')
export class GunsController {
    constructor(private readonly gunsService: GunsService) {}
    @Get('/getGunsTypes')
    findALl() {
        return this.gunsService.getGunsTypes();
    }
}
