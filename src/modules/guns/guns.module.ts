import { Module } from '@nestjs/common';
import { GunsController } from './guns.controller';
import { GunsService } from './guns.service';

@Module({
    controllers: [GunsController],
    providers: [GunsService],
})
export class GunsModule {}
