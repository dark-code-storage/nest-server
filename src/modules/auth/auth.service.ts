import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { InjectRedis, Redis } from '@nestjs-modules/ioredis';
import { ConfigService } from '@nestjs/config';
import { UsersService } from '../users/users.service';
import { USER_TOKEN_KEY } from '@/common/constants/redis.constant';
import { ApiException } from '@/common/class/ajax-error-result';

@Injectable()
export class AuthService {
    constructor(
        @InjectRedis() private readonly redis: Redis,
        private readonly configService: ConfigService,
        @Inject(forwardRef(() => UsersService))
        private readonly userService: UsersService,
    ) {}

    // 判断token 是否过期 或者被重置
    async validateToken(username: string, resToken: string) {
        const token = await this.redis.get(`${USER_TOKEN_KEY}:${username}`);
        if (resToken !== token) throw new ApiException(12002);
    }
}
