import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { UsersService } from '@/modules/users/users.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly usersService: UsersService) {
        super({
            usernameField: 'username',
            passwordField: 'password',
        });
    }
    // 用户登录进行校验
    async validate(username: string, password: string): Promise<UserDetailResult> {
        const user = await this.usersService.validateUser(username, password);
        return user;
    }
}
