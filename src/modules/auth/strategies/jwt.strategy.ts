import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';
import { ApiException } from '@/common/class/ajax-error-result';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly configService: ConfigService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: true,
            secretOrKey: configService.get('jwt.secret'),
        });
    }

    async validate(payload) {
        const currentTime = Date.now() / 1000;
        if (payload.exp > currentTime) {
            return payload; //返回值会被 守卫的  handleRequest方法 捕获
        } else {
            // token身份过期
            throw new ApiException(12002);
        }
    }
}
