import { Module, forwardRef } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategies/jwt.strategy';
import { LocalStrategy } from './strategies/local.strategy';
import { PassportModule } from '@nestjs/passport';
import { UsersModule } from '../users/users.module';

@Module({
    imports: [PassportModule, forwardRef(() => UsersModule)],
    providers: [AuthService, LocalStrategy, JwtStrategy],
})
export class AuthModule {}
