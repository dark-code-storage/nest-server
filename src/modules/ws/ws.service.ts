import { Injectable } from '@nestjs/common';
import { WsGateway } from './gateway/ws.gateway';
import { DISPATCH_EVENT_NOTICE } from '@/common/constants/ws.constant';

@Injectable()
export class WsService {
    constructor(private wsGateway: WsGateway) {}

    //推送通知内容
    dispatchNotice(notice) {
        this.wsGateway.socketServer.emit(DISPATCH_EVENT_NOTICE, {
            message: notice,
        });
    }
}
