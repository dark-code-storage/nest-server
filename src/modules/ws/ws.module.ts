import { Module } from '@nestjs/common';
import { WsService } from './ws.service';
import { WsGateway } from './gateway/ws.gateway';
import { ReceptionGateway } from './gateway/reception.gateway';

const providers = [WsService, WsGateway, ReceptionGateway];
@Module({
    providers: providers,
    exports: providers,
})
export class WsModule {}
