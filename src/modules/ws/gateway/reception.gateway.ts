import { CustomSocket } from '@/common/adapter/ws.adapter';
import { RECEPTION_EVENT_NOTICE } from '@/common/constants/ws.constant';
import { SubscribeMessage, WebSocketGateway } from '@nestjs/websockets';

/**
 * 接收ws响应
 */
@WebSocketGateway()
export class ReceptionGateway {
    @SubscribeMessage(RECEPTION_EVENT_NOTICE)
    notice(socket: CustomSocket, data) {
        console.log(data);
    }
}
