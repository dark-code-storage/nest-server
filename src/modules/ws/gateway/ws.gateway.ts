import { OnModuleInit } from '@nestjs/common';
import { WebSocketGateway, WebSocketServer, OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { Server } from 'socket.io';
import log from '@/utils/log';
import { CustomSocket } from '@/common/adapter/ws.adapter';
import { DISPATCH_EVENT_ONLINE, DISPATCH_EVENT_OFFLINE } from '@/common/constants/ws.constant';

@WebSocketGateway()
export class WsGateway implements OnGatewayConnection, OnGatewayDisconnect, OnModuleInit {
    @WebSocketServer()
    server: Server;

    get socketServer(): Server {
        return this.server;
    }

    async onModuleInit() {
        log.success('websocket', '      - 初始化执行');
    }

    async handleConnection(socket: CustomSocket) {
        log.success('websocket', '      - 已连接');
        socket.emit(DISPATCH_EVENT_ONLINE);
    }

    async handleDisconnect(socket: CustomSocket) {
        log.error('websocket', '        - 已断开');
        this.disconnect(socket);
    }

    private disconnect(socket: CustomSocket) {
        socket.emit(DISPATCH_EVENT_OFFLINE);
        socket.disconnect();
    }
}
