import { IsNotEmpty } from 'class-validator';

export class CreateUserDto {
    @IsNotEmpty({
        message: 'username不能为空',
    })
    username: string;

    @IsNotEmpty({
        message: 'password不能为空',
    })
    password: string;
}

export class LoginDto {
    @IsNotEmpty({
        message: 'username不能为空',
    })
    username: string;

    @IsNotEmpty({
        message: 'password不能为空',
    })
    password: string;
}

export class LoginDtoResultDto {
    /* token密匙 */
    token: string;
}
