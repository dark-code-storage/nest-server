import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { hashSync } from 'bcryptjs';
import { BaseSchema } from '@/common/schemas/base.schemas';
import { HydratedDocument } from 'mongoose';

export type UserDocument = HydratedDocument<User>;

@Schema()
export class User extends BaseSchema {
    @Prop()
    username: string;

    @Prop({
        required: true,
        select: false,
        set(value) {
            return hashSync(value, 12);
        },
    })
    password: string;
}
export const UserSchema = SchemaFactory.createForClass(User);
