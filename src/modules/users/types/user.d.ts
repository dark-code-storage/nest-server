interface UserDetailResult {
    _id: ObjectId;
    username: string;
}

/**
 * @description 带有用户密码的详情
 */
interface UserInfoResult {
    _id: ObjectId;
    username: string;
    password: string;
}
