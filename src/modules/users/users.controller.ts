import { Controller, Post, Body, Get, Request, UseGuards } from '@nestjs/common';
import { CreateUserDto, LoginDto, LoginDtoResultDto } from './dto/users.dto';
import { UsersService } from './users.service';
import { Public } from '@/common/decorators/public.decorator';
import { RepeatSubmit } from '@/common/decorators/repeat-submit.decorator';
import { LocalAuthGuard } from '@/common/guards/local-auth.guard';

@Controller('user')
export class UserController {
    constructor(private readonly userService: UsersService) {}

    @Post('/create')
    create(@Body() createUserDto: CreateUserDto) {
        return this.userService.createUser(createUserDto);
    }
    @Public()
    @UseGuards(LocalAuthGuard)
    @RepeatSubmit()
    @Post('/login')
    login(@Body() loginDto: LoginDto, @Request() request): Promise<LoginDtoResultDto> {
        return this.userService.login(request);
    }

    @Get('/getUserInfo')
    getUserInfo(@Request() request) {
        return this.userService.getUserInfo(request);
    }
}
