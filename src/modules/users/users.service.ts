import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { compareSync } from 'bcryptjs';
import { User, UserDocument } from './schemas/user.schemas';
import { CreateUserDto } from './dto/users.dto';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { JwtService } from '@nestjs/jwt';
import Redis from 'ioredis';
import { ApiException } from '@/common/class/ajax-error-result';

@Injectable()
export class UsersService {
    constructor(
        @InjectRedis()
        private readonly redis: Redis,
        private readonly jwtService: JwtService,
        @InjectModel(User.name)
        private readonly userModal: Model<UserDocument>,
    ) {}

    // 创建用户
    async createUser(createUserDto: CreateUserDto) {
        await this.userModal.create({
            ...createUserDto,
        });
    }

    // 用户登录
    async login(request) {
        const user: UserDetailResult = request.user;
        const jwtSign = this.jwtService.sign({
            username: user.username,
            userId: user._id,
        });
        return { token: jwtSign ?? '' };
    }

    //获取用户信息
    async getUserInfoByUserName(username: string): Promise<UserInfoResult> {
        const user = await this.userModal
            .findOne({
                username,
            })
            .select('+password')
            .exec();

        return user;
    }

    //获取用户信息
    async getUserInfoById(_id: string): Promise<UserDetailResult> {
        const user = await this.userModal.findById(_id).exec();
        return user;
    }

    // 校验用户登录
    async validateUser(username: string, password: string): Promise<UserDetailResult> {
        // 判断用户是否存在
        const user = await this.getUserInfoByUserName(username);
        if (!user) {
            throw new ApiException(11001);
        }
        // 判断用户账号密码是否正确
        const isValid = compareSync(password, user.password);
        if (!isValid) {
            throw new ApiException(11002);
        }
        return user;
    }

    //获取用户详情
    async getUserInfo(request): Promise<UserDetailResult> {
        const { userId } = request.user;
        return await this.getUserInfoById(userId);
    }
}
