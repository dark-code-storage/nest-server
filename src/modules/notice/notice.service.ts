import { Injectable } from '@nestjs/common';
import { CreateNoticeDto } from './dto/notice.dto';
import { WsService } from '../ws/ws.service';

@Injectable()
export class NoticeService {
    constructor(private readonly wsService: WsService) {}
    async createNotice(createNoticeDto: CreateNoticeDto) {
        this.wsService.dispatchNotice(createNoticeDto.message);

        return '';
    }
}
