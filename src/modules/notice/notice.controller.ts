import { Body, Controller, Post } from '@nestjs/common';
import { NoticeService } from './notice.service';
import { CreateNoticeDto } from './dto/notice.dto';

@Controller('notice')
export class NoticeController {
    constructor(private readonly noticeService: NoticeService) {}

    @Post('create')
    createNotice(@Body() createNoticeDto: CreateNoticeDto) {
        return this.noticeService.createNotice(createNoticeDto);
    }
}
