import { IsNotEmpty } from 'class-validator';

export class CreateNoticeDto {
    @IsNotEmpty({
        message: 'title不能为空',
    })
    title: string;

    @IsNotEmpty({
        message: 'message不能为空',
    })
    message: string;
}
