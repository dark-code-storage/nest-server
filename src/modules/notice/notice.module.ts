import { Module } from '@nestjs/common';
import { WsModule } from '../ws/ws.module';

@Module({})
export class NoticeModule {
    import: [WsModule];
}
