import { MiddlewareConsumer, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoggerMiddleware } from './common/middleware/logger.middleware';
import { ConfigModule } from '@nestjs/config';
import { getConfiguration } from './config';
import { UsersModule } from '@/modules/users/users.module';
import { GlobalModule } from './common/modules/global.module';
import { AuthModule } from './modules/auth/auth.module';
import { GunsModule } from './modules/guns/guns.module';
import { WsModule } from './modules/ws/ws.module';
import { NoticeController } from './modules/notice/notice.controller';
import { NoticeService } from './modules/notice/notice.service';
import { NoticeModule } from './modules/notice/notice.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
            load: [getConfiguration],
            envFilePath: [`.env.${process.env.NODE_ENV}`, '.env'],
        }),
        AuthModule,
        UsersModule,
        GlobalModule,
        GunsModule,
        WsModule,
        NoticeModule,
    ],
    controllers: [AppController, NoticeController],
    providers: [AppService, NoticeService],
})
export class AppModule {
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(LoggerMiddleware).forRoutes('*');
    }
}
